/*
 * 
 * @file		:  hashlp.c
 * @brief		:  Hashing program using linear probing
 * @version		:  1.0
 * @created		:  Thursday 18 February 2021 10:04:42  IST
 * @revision	:  none
 * @compiler	:  gcc
 * @author		:  Sanjeevkumar Palanisamy  sanjeevkumar.palanisamy@vvdntech.in
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SUCCESS 0
#define FAILURE -1
#define MAX 10                                  /* Maximum entries in hash table */


struct myhashtable {
	int key;
	int val;
};				/* ----------  end of struct hashtable  ---------- */

typedef struct myhashtable myhashtable_t;
myhashtable_t mytable[10];
int table_size = SUCCESS;

void add ();
void delete ();
void display ();
int inpValid (char *msg);
int hashFunc (int num);

/* 
 *
 * @function	:  main
 * @description :  Main function of the program
 * @param1		:  argc - No.of arguments
 * @param2		:  *argv[] - Array of pointers to arguments
 * @return		:  Exit status
 *
 */

int main ( int argc, char *argv[] )
{ 
	int choice = 0;
	memset (mytable, FAILURE , sizeof mytable); 		/* Set the value to -1 for whole table */
	
	for ( int exit = 0; exit != 1 ; )
	{
		choice = inpValid ( "\n1->Add\n2->Delete\n3->Display\n4->Size of table\n5->Exit\nEnter your choice:" );

		switch ( choice ) {
			case 1 :
				add ();
				break;

			case 2 :
				delete ();
				break;

			case 3 :
				display ();
				break;

			case 4 :
				printf ( "\nSize of table: %d", table_size );
				break;

			case 5 :
				exit = 1;
				break;

			default :
				printf ( "\nWrong option!!!" );	
				break;
		}				/* -----  end switch  ----- */
	}					/* -----  end for loop ----- */
		return SUCCESS;
}				/* ----------  end of function main  ---------- */


/* 
 *
 * @function	:  hashFunc
 * @description :  Compute the hash of the given value
 * @param1		:  num - Number to which the hash value to be computed
 * @return		:  hash of the given value
 * 
 */

int hashFunc ( int num )
{
	return (num % MAX);                         /* Returns the computed hash value */
}		/* -----  end of function hashFunc  ----- */

/*
 * @function    : inpValid 
 * @description : Checks the user input as integer only
 * @param1      : msg - Message to print when called
 * @return		: Interger Option given by the user
 */

int inpValid(char *msg)
{
	int res;
	char *endptr;
	char str[10];
	for(;;)
	{
		printf("%s",msg);                       /* Prints the given message */
		fgets(str,10,stdin);
		res = strtol(str, &endptr,10);		/*stores the integer entered by user*/
		if (*endptr=='\n')
			return res;                         /* Return the stored integer */
		else 
			printf("\nWrong option!!!\n");
	}					/* -----  end for loop ----- */
}


/* 
 *
 * @function	:  add
 * @description :  Adds an entry to hash table
 * @param1		:  void
 * @return		:  void
 * 
 */

void add (  )
{
	if ( table_size >= MAX ){                   /* Checks the table size should not be greater than MAX*/
		printf ( "\nMaximum table size reached!!!\nDelete an entry to add." );
		return ;
	}
	int loop = 0;
	int key = inpValid ( "\nEnter the key:" );  /* Get the input from user */
	int val = inpValid ( "\nEnter the value:" );
	int hash = hashFunc ( key );                /* Compute the hash of the key */

	for ( loop = hash ; mytable[loop].key != FAILURE && loop< MAX; ++loop );				/* -----  end for loop ----- */
	if (loop >= MAX){
			for ( loop = 0 ; mytable[loop].key != FAILURE && loop< hash; ++loop );			/* -----  end for loop ----- */
	}
	mytable[loop].key = key;
	mytable[loop].val = val;
	++table_size;
	return ;
}		/* -----  end of function add  ----- */


/* 
 *
 * @function	:  delete
 * @description :  Delete an entry from hash table
 * @param1		:  void
 * @return		:  void
 * 
 */

void delete (  )
{

	if ( table_size == 0 ){                      /* Checks if table is empty */
		printf ( "\nNO entries to delete!!!" );
		return ;
	}
	int loop = 0;
	int key = inpValid ( "\nEnter the key:" );
	int hash = hashFunc ( key );                /* Compute the hash of the key */

	for ( loop = hash ; mytable[loop].key != key && loop < MAX; ++loop );			/* -----  end for loop ----- */
	
	if (loop >= MAX){
		for ( loop = 0 ; mytable[loop].key != key && loop < hash; ++loop );		/* -----  end for loop ----- */
	}

	if (mytable[loop].key == key){              /* Delete the entry */
		mytable[loop].key = -1;
		mytable[loop].val = -1;
		--table_size;
		printf ( "\nDeleted successfully" );
		return ;
	}
	printf ( "\nDeletion unsuccessful!!!" );
	return ;
}		/* -----  end of function delete  ----- */



/* 
 *
 * @function	:  display
 * @description :  Display all the entries from hash table
 * @param1		:  void
 * @return		:  void
 * 
 */

void display (  )
{
	if ( table_size < 0 ){                      /* Checks if the table is empty */
		printf ( "\nNO entries to display!!!" );
		return ;
	}
	for (int loop = 0 ; loop < MAX ; ++loop )   	/* Loop through the hash table and print the elements */
	{
		if (  mytable[loop].key != FAILURE ){   	/* Checks if the key is NULL */
			printf ( "\nIndex:%d Key: %d", loop, mytable[loop].key);
			printf ( "\tValue: %d", mytable[loop].val);
		}
	}	 /* -----  end for loop ----- */
	return ;
}		/* -----  end of function display  ----- */
